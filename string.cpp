// string.cpp
// Cameron White
// This file contains the implementation String class.
#include "string.h" 
#include <cstring>

String::~String() {
  delete[] string;
}
String::String() {
  string = NULL;
}
String::String(const char *string) {
  this->string = NULL;
  *this = string;
}
String::String(const String &string) {
  this->string = NULL;
  *this = string;
}
int String::length() const {
  if (string == NULL)
    return strlen(string);
  else
    return 0;
}
const char* String::c_str() const {
  return (const char*) string; // TODO change to c++ cast
}
char* String::data() const {
  int len = length();
  char *copy = new char[len+1];
  memset((void*) copy, (int) '\0', len+1);
  strncpy(copy, string, len);
  return copy;
}
String& String::operator =(const String &rhs) {
  return *this = rhs.c_str();
}
String& String::operator =(const char *rhs) {
  int len = 0;
  delete[] string;
  string = NULL;
  if (rhs != NULL) 
    len = strlen(rhs);
  if (len > 0) {
    string = new char[len+1];
    strncpy(string, rhs, len+1);
  }
  return *this;
}
String String::operator +(const String &rhs) const {
  return *this + rhs.c_str();
}
String String::operator +(const char *rhs) const {
  char *temp = concatenate(*this, rhs);
  String newString;
  newString.set(temp);
  return newString;
}
String& String::operator +=(const String &rhs) {
  return *this += rhs.c_str();
}
String& String::operator +=(const char *rhs) {
  char *temp = concatenate(*this, rhs);
  delete[] string;
  string = temp;
  return *this;
}
bool String::operator ==(const String &rhs) const {
  return *this == rhs.c_str();
}
bool String::operator ==(const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  if (strncmp(string, rhs, lenL+lenR) == 0)
    return true;
  else 
    return false;
}
bool String::operator !=(const String &rhs) const {
  return *this != rhs.c_str();
}
bool String::operator !=(const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  if (strncmp(string, rhs, lenL+lenR) != 0)
    return true;
  else 
    return false;
}
bool String::operator <(const String &rhs) const {
  return *this < rhs.c_str();
}
bool String::operator <(const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  if (strncmp(string, rhs, lenL+lenR) < 0)
    return true;
  else 
    return false;
}
bool String::operator <=(const String &rhs) const {
  return *this <= rhs.c_str();
}
bool String::operator <=(const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  if (strncmp(string, rhs, lenL+lenR) <= 0)
    return true;
  else 
    return false;
}
bool String::operator >(const String &rhs) const {
  return *this > rhs.c_str();
}
bool String::operator >(const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  if (strncmp(string, rhs, lenL+lenR) > 0)
    return true;
  else 
    return false;
}
bool String::operator >=(const String &rhs) const {
  return *this >= rhs.c_str();
}
bool String::operator >=(const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  if (strncmp(string, rhs, lenL+lenR) >= 0)
    return true;
  else 
    return false;
}
char* String::concatenate(const String &lfs, const char *rhs) const {
  int lenL = length(),
      lenR = strlen(rhs);
  char *string = new char[lenL+lenR+1]; // +1 for '\0'
  memset((void*) string, (int) '\0', lenL+lenR+1);
  strncpy(string, c_str(), lenL);
  strncat(string, rhs, lenL+lenR);
  return string;
}
void String::set(char *string) {
  this->string = string;
}
