// _DListIterator.cpp
// Cameron White
// This file contains the implementation of the _DListIterator 
// class.
#include "_DListIterator.h"
#include "_DList.h"
#include "dList.h"
#include "dListIterator.h"
#include "string.h"

// Default constructor
template<class DATA, class NODE, class ITERATOR> 
_DListIterator<DATA,NODE,ITERATOR>::_DListIterator(NODE *node) 
  : _SListIterator<DATA,NODE,ITERATOR>(node) {
}

// Template Prototypes
template class _DListIterator<int, DNode<int>, DListIterator<int> >;
template class _DListIterator<String, DNode<String>, DListIterator<String> >;
