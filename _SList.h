// _SList.h
// Cameron White
// This file contains the interface of the _SList class. Use this class
#ifndef SLISTPRIVATE_H
#define SLISTPRIVATE_H
#include "_Array.h"
#include <cstdlib>

// This class is a private class. Its purpose is to template key
// data types in order to allow its virtual functions to be valid 
// in any derived version. 
template<class DATA, class NODE, class LIST, class ITERATOR> 
class _SList : public _Array<DATA,NODE,LIST,ITERATOR> {
 public:
  // Default constructor
  _SList(void (*deleteData)(DATA)=NULL);
  virtual Container::Status prepend(DATA &data)=0;
  //virtual operator =(const LIST& rhs)=0;
};

#endif // SLISTPRIVATE_H

