// _Array.cpp
// Cameron White
// This file contains the implementation of the _Array class.
#include "_Array.h"
#include "sList.h"
#include "dList.h"
#include "sListIterator.h"
#include "dListIterator.h"
#include "sCircleListIterator.h"
#include "sCircleList.h"
#include "flexibleArrayIterator.h"
#include "flexibleArray.h"
#include "activity.h"
#include "string.h"

// Default Constructor
template<class DATA, class NODE, class LIST, class ITERATOR>
_Array<DATA,NODE,LIST,ITERATOR>::_Array(void (*deleteData)(DATA)) 
  : front(NULL),
    back(NULL),
    deleteData(deleteData) {
}

// Template Prototypes
template class _Array<int, SNode<int>, 
                      SList<int>, SListIterator<int> >;
template class _Array<Activity, SNode<Activity**>, 
                      FlexibleArray<Activity,8>, 
                      FlexibleArrayIterator<Activity,8> >;
template class _Array<Activity*, SNode<Activity***>, 
                      FlexibleArray<Activity*,8>, 
                      FlexibleArrayIterator<Activity*,8> >;
template class _Array<int, SNode<int**>, 
                      FlexibleArray<int,2>, 
                      FlexibleArrayIterator<int,2> >;
template class _Array<int, SNode<int>, 
                      SCircleList<int>, 
                      SCircleListIterator<int> >;
template class _Array<int, DNode<int>, 
                      DList<int>, DListIterator<int> >;
template class _Array<String, DNode<String>, 
                      DList<String>, DListIterator<String> >;
template class _Array<String, SNode<String>, 
                      DList<String>, DListIterator<String> >;
template class _Array<String, SNode<String>, 
                      SList<String>, DListIterator<String> >;
template class _Array<String, DNode<String>, 
                      SList<String>, DListIterator<String> >;
template class _Array<String, DNode<String>, 
                      DList<String>, SListIterator<String> >;
template class _Array<String, SNode<String>, 
                      DList<String>, SListIterator<String> >;
template class _Array<String, SNode<String>, 
                      SList<String>, SListIterator<String> >;
template class _Array<String, DNode<String>, 
                      SList<String>, SListIterator<String> >;
template class _Array<String, SNode<String>, 
                      SCircleList<String>, SCircleListIterator<String> >;
