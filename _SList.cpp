// sList.cpp
// Cameron White
// This file contains the implementation of the SList class.
#include "sList.h"
#include "dList.h"
#include "sListIterator.h"
#include "dListIterator.h"
#include "sCircleListIterator.h"
#include "sCircleList.h"
#include "string.h"

template<class DATA, class NODE, class LIST, class ITERATOR> 
_SList<DATA,NODE,LIST,ITERATOR>::_SList(void (*deleteData)(DATA))
  : _Array<DATA,NODE,LIST,ITERATOR>(deleteData) {
}
template class _SList<int, SNode<int>, SList<int>, 
                      SListIterator<int> >;
template class _SList<int, SNode<int>, SCircleList<int>, 
                      SCircleListIterator<int> >;
template class _SList<int, DNode<int>, DList<int>, 
                      DListIterator<int> >;
template class _SList<String, SNode<String>, DList<String>, 
                      DListIterator<String> >;
template class _SList<String, DNode<String>, DList<String>, 
                      DListIterator<String> >;
template class _SList<String, SNode<String>, SList<String>, 
                      DListIterator<String> >;
template class _SList<String, DNode<String>, SList<String>, 
                      DListIterator<String> >;
template class _SList<String, SNode<String>, DList<String>, 
                      SListIterator<String> >;
template class _SList<String, DNode<String>, DList<String>, 
                      SListIterator<String> >;
template class _SList<String, SNode<String>, SList<String>, 
                      SListIterator<String> >;
template class _SList<String, DNode<String>, SList<String>, 
                      SListIterator<String> >;
template class _SList<String, SNode<String>, SCircleList<String>, 
                      SCircleListIterator<String> >;
