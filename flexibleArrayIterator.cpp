// flexibleArrayIterator.cpp
// Cameron White
// This file contains the implementation of the 
// FlexibleArrayIterator class.
#include "flexibleArrayIterator.h"
#include "flexibleArray.h"
#include "activity.h"

template<class T, int N> 
FlexibleArrayIterator<T,N>::FlexibleArrayIterator() 
  : index(0) {
}
// This operator iterates to the next item. Each node in the
// FlexableList conains an array. The operator will set the current
// position to the next item in the contained array. If the next
// position exceeds the length array the operator will start at the
// beginning of the next node if possible.
// TODO this is a mess.
template<class T, int N>
FlexibleArrayIterator<T,N> 
FlexibleArrayIterator<T,N>::operator ++(int) {
  T **dataArray;
  if (this->node != NULL) {
    if (this->index < N) {
      index++;
      this->node->getData(dataArray);
      if (dataArray[index] == NULL) {
        this->node = NULL;
        index = 0;
      }
    } else {
      for (;;) {
        this->node->getNext(&(this->node));
        this->index = 0;
        this->node->getData(dataArray);
        if (dataArray[index] == NULL) {
          this->node = NULL;
          index = 0;
        } else
          break;
      }
    }
  }
  return *this;
}
// This operator will get the data Array contained in the node
// referenced by the iterator. It will then return the data at 
// index.
template<class T, int N>
T FlexibleArrayIterator<T,N>::operator *() {
  T **dataArray;
  if (this->node) {
    if (index < N) {
      this->node->getData(dataArray);
      if (dataArray[index])
        return *dataArray[index];
    }
  }

  ; // TODO Throw exception
}
template<class T, int N>
bool FlexibleArrayIterator<T,N>::operator ==(
    const FlexibleArrayIterator<T,N> &rhs) {
  if (this->node == rhs.node && this->index == rhs.index)
    return true;
  return false;
}
template<class T, int N>
bool FlexibleArrayIterator<T,N>::operator !=(
    const FlexibleArrayIterator<T,N> &rhs) {
  if (this->node != rhs.node || this->index != rhs.index)
    return true;
  return false;
}
template<class T, int N>
FlexibleArrayIterator<T,N>& FlexibleArrayIterator<T,N>::operator =(
    const FlexibleArrayIterator<T,N> &rhs) {
  this->node = rhs.node;
  this->index = rhs.index;
  return *this;
}
template<class T, int N>
unsigned FlexibleArrayIterator<T,N>::getIndex() {
  return this->index;
}
template<class T, int N>
void FlexibleArrayIterator<T,N>::setIndex(unsigned index) {
  this->index = index;
}

template class FlexibleArrayIterator<int, 2>;
template class FlexibleArrayIterator<Activity, 8>;
template class FlexibleArrayIterator<Activity*, 8>;
