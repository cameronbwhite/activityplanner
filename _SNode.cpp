// _SNode.cpp
// Cameron White
// This file contains the implementation to the _SNode Class.
#include "_SNode.h"
#include "sNode.h"
#include "dNode.h"
#include "flexibleArray.h"
#include "activity.h"
#include "string.h"

template<class DATA, class NODE> 
_SNode<DATA,NODE>::_SNode() 
  : next(NULL) {
}
template<class DATA, class NODE>
_SNode<DATA,NODE>::_SNode(
    DATA &data, NODE *next, void (*deleteData)(DATA))
  : _Node<DATA,NODE>(data,deleteData),
    next(next) {
}
template<class DATA, class NODE>
typename Container::Status 
_SNode<DATA,NODE>::setNext(NODE *next) {
  this->next = next;
  return this->SUCCESS;
}
template<class DATA, class NODE>
typename Container::Status 
_SNode<DATA,NODE>::getNext(NODE **next) {
  *next = this->next;
  return this->SUCCESS;
}
template<class DATA, class NODE>
NODE& _SNode<DATA,NODE>::operator =(const NODE &rhs) {
  this->_Node<DATA,NODE>::operator=(rhs);
  this->next = rhs.next;
  return static_cast<NODE&>(*this);
}
template<class DATA, class NODE>
bool _SNode<DATA,NODE>::operator ==(const NODE &rhs) {
  if (this->_Node<DATA,NODE>::operator ==(rhs))
    if (this->next == rhs.next)
      return true;
  return false;
}
template<class DATA, class NODE>
bool _SNode<DATA,NODE>::operator !=(const NODE &rhs) {
  if (this->_Node<DATA,NODE>::operator !=(rhs))
    return true;
  else if (this->next != rhs.next)
    return true;
  return false;
}

template class _SNode<int, SNode<int> >;
template class _SNode<int**, SNode<int**> >;
template class _SNode<int, DNode<int> >;
template class _SNode<String, SNode<String> >;
template class _SNode<String, DNode<String> >;
template class _SNode<Activity**, SNode<Activity**> >;
template class _SNode<Activity***, SNode<Activity***> >;
