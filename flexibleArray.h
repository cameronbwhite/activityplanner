// flexibleArray.h
// Cameron White
// This file contains the interface of the FlexibleArray class.
// Use this class to store items in a flexible array.
#ifndef FLEXABLE_ARRAY_H
#define FLEXABLE_ARRAY_H
#include "flexibleArrayIterator.h"
#include "sNode.h"
#include "_Array.h"

// T is the item type stored in the FlexibleArray and N is the size of
// each array. 
template<class T, int N> class FlexibleArray 
  : public _Array<T, SNode<T**>, FlexibleArray<T,N>, FlexibleArrayIterator<T,N> > {
 public:
  ~FlexibleArray();
  // Default Constructor
  FlexibleArray(void (*deleteData)(T)=NULL);
  // Add item at the end of the array.
  virtual Container::Status append(T &data);
  // Sets the iterator to reference the start of the array.
  virtual Container::Status begin(FlexibleArrayIterator<T,N>& iterator);
  // Sets the iterator to reference on past the end of the array.
  virtual Container::Status end(FlexibleArrayIterator<T,N>& iterator);
 protected:
  SNode<T**>* createNode(SNode<T**> *next=NULL);
  Container::Status setDataArray(T &data, T **dataArray, int i);
};

#endif // FLEXABLE_ARRAY_H
