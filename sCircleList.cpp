// circleList.cpp
// Cameron White
// This file contains the implementation of the SCircleList class.
#include "sCircleList.h"
#include "string.h" 

template<class T>
SCircleList<T>::~SCircleList() {
  SNode<T> *temp;
  // Ensure that iteration stops after the back node.
  if (this->back != NULL)
    this->back->setNext(NULL);
  // Delete every node in the list.
  while (this->front) {
    this->front->getNext(&temp);
    delete this->front;
    this->front = temp;
  }
}
// Default Constructor
template<class T>
SCircleList<T>::SCircleList(void (*deleteData)(T))
  : _SList<T,SNode<T>, SCircleList<T>, 
                 SCircleListIterator<T> >(deleteData) {
}
template<class T>
typename Container::Status SCircleList<T>::append(T &data) {
  SNode<T> *newNode = new SNode<T>(data, 
                                           this->front, 
                                           this->deleteData);
  if (this->front == NULL)
    this->front = newNode;

  if (this->back == NULL) {
    this->back = newNode;
    newNode->setNext(newNode);
  } else {
    static_cast<SNode<T>*>(this->back)->setNext(newNode);
    this->back = newNode;
  } 

  this->size++;

  return this->SUCCESS;
}
template<class T>
typename Container::Status SCircleList<T>::prepend(T &data) {
  SNode<T> *newNode = new SNode<T>(data, 
                                           this->front,
                                           this->deleteData);
  this->front = newNode;

  if (this->back == NULL)
    this->back = newNode;
  else 
    static_cast<SNode<T>*>(this->back)->setNext(newNode);

  this->size++;

  return this->SUCCESS;
}
template<class T>
Container::Status SCircleList<T>::begin(SCircleListIterator<T> &iterator) {
  iterator.setNode(this->front);
  return this->SUCCESS;
}
template<class T>
Container::Status SCircleList<T>::end(SCircleListIterator<T> &iterator) {
  iterator.setNode(this->front);
  return this->SUCCESS;
}

template class SCircleList<int>;
template class SCircleList<String>;
