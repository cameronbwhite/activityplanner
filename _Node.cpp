// _Node.cpp
// Cameron White
// This file contains the implementation of the _Node class.
#include "_Node.h"
#include "dNode.h"
#include "sNode.h"
#include "string.h"
#include "activity.h"

// If the user specifed a deleteData function (i.e the function 
// pointer is non Null) call the function with the contained data.
template<class DATA, class NODE>
_Node<DATA,NODE>::~_Node() {
  if (deleteData)
    deleteData(data);
}
// Default constructor
template<class DATA, class NODE>
_Node<DATA,NODE>::_Node(void (*deleteData)(DATA)) 
  : deleteData(deleteData) {
}
// Constructor
template<class DATA, class NODE>
_Node<DATA,NODE>::_Node(DATA &data, 
                                    void (*deleteData)(DATA)) 
  : data(data),
    deleteData(deleteData) {
}
// If the user specifed a deleteData function (i.e the function
// pointer is non Null) the function is called with the contained 
// data before the data is set to the new data.
template<class DATA, class NODE>
typename _Node<DATA,NODE>::Status 
_Node<DATA,NODE>::setData(DATA &data) {
  if (deleteData)
    deleteData(this->data);
  this->data = data;
  return SUCCESS;
}
// If the user specifed a deleteData function (i.e the function
// pointer is non Null) the function is called with the contained 
// data before the data is set to the new data.
template<class DATA, class NODE>
typename _Node<DATA,NODE>::Status 
_Node<DATA,NODE>::setData(DATA *data) {
  if (deleteData)
    deleteData(this->data);
  this->data = *data;
  return SUCCESS;
}
template<class DATA, class NODE>
typename _Node<DATA,NODE>::Status 
_Node<DATA,NODE>::getData(DATA &data) {
  data = this->data;
  return SUCCESS;
}
template<class DATA, class NODE>
typename _Node<DATA,NODE>::Status 
_Node<DATA,NODE>::getData(DATA **data) {
  *data = &(this->data);
  return SUCCESS;
}
template<class DATA, class NODE>
NODE& _Node<DATA,NODE>::operator =(const NODE &rhs) {
  this->data = rhs.data;
  this->deleteData = rhs.deleteData;
  return static_cast<NODE&>(*this);
}
template<class DATA, class NODE>
bool _Node<DATA,NODE>::operator ==(const NODE &rhs) {
  if (data == rhs.data && deleteData == rhs.deleteData)
    return true;
  else
    return false;
}
template<class DATA, class NODE>
bool _Node<DATA,NODE>::operator !=(const NODE &rhs) {
  if (data != rhs.data || deleteData != rhs.deleteData)
    return true;
  else
    return false;
}

// Template Prototypes
template class _Node<int, SNode<int> >;
template class _Node<int**, SNode<int**> >;
template class _Node<Activity**, SNode<Activity**> >;
template class _Node<Activity***, SNode<Activity***> >;
template class _Node<int, DNode<int> >;
template class _Node<String, DNode<String> >;
template class _Node<String, SNode<String> >;
