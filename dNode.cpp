// dNode.cpp
// Cameron White
// This file contains the implementation of the DNode class.
#include "dNode.h"
#include "string.h"

// Default constructor
template<class T>
DNode<T>::DNode() {
}
template<class T>
DNode<T>::DNode(T &data, DNode<T> *previous, 
                      DNode<T> *next, void (*deleteData)(T)) 
  : _DNode<T, DNode<T> >(data, previous, next, deleteData) {
}

template class DNode<int>;
template class DNode<String>;
