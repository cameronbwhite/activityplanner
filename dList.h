// dList.h
// Cameron White
// This file contains the iterface of the List class.
// Use this class to store data in a Double linked list linked.
#ifndef LIST_H
#define LIST_H
#include "_DList.h"
#include "dNode.h"
#include "dListIterator.h"
#include "sList.h"
#include <cstdlib>

template<class T> class DNode;
template<class T> class DListIterator;

// This class is a doubly linked linear list.
template<class T> 
class DList 
  : public _DList<T, DNode<T>, DList<T>, DListIterator<T> > {
 public:
  ~DList();
  // Default constructor
  DList(void (*deleteData)(T)=NULL);
  // Add an item to the end of the list.
  virtual Container::Status append(T &data);
  // Add an item to the beginning of the list.
  virtual Container::Status prepend(T &data);
  // Set the iterator to reference to the beginning of the list.
  virtual Container::Status begin(DListIterator<T>& iterator);
  // Set the iterator to end to the beginning of the list.
  virtual Container::Status end(DListIterator<T>& iterator);
 protected:
 private:
};

#endif // LIST_H
