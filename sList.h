// list.h
// Cameron White
// This file contains the iterfaces of the SList class.
#ifndef SLIST_H
#define SLIST_H
#include "_SList.h"
#include "sListIterator.h"
#include "sNode.h"
#include "dListIterator.h"
#include <cstdlib>

template<class T> class SNode;
template<class T> class SListIterator;

// This class is a singly linked linear list. 
template<class T> 
class SList 
  : public _SList<T, SNode<T>, SList<T>, 
           SListIterator<T> > {
 public:
  ~SList();
  // Default constructor
  SList(void (*deleteData)(T)=NULL);
  virtual Container::Status append(T &data);
  virtual Container::Status prepend(T &data);
  virtual Container::Status begin(SListIterator<T>& iterator);
  virtual Container::Status end(SListIterator<T>& iterator);
  //virtual SList<T>& operator =(const SList<T>& rhs)=0;
 protected:
 private:
};

#endif // SLIST_H
