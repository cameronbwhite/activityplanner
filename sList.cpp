// sList.cpp
// Cameron White
// This file contains the implementation of the SList class.
#include "sList.h"
#include "sNode.h"
#include "string.h"

template<class T>
SList<T>::~SList() {
  SNode<T> *temp;
  // Ensure that iteration stops after the back node.
  if (this->back != NULL)
    this->back->setNext(NULL);
  // Delete every node in the list.
  while (this->front) {
    this->front->getNext(&temp);
    delete this->front;
    this->front = temp;
  }
}
// Default constructor
template<class T>
SList<T>::SList(void (*deleteData)(T)) 
  : _SList<T, SNode<T>, SList<T>,
           SListIterator<T> >(deleteData) {
}
template<class T>
Container::Status SList<T>::append(T &data) {
  SNode<T> *newNode = new SNode<T>(data, NULL, this->deleteData);

  if (this->front == NULL)
    this->front = newNode;

  if (this->back != NULL)
    this->back->setNext(newNode);
  this->back = newNode;
  
  this->size++;

  return this->SUCCESS;
}
template<class T>
Container::Status SList<T>::prepend(T &data) {
  SNode<T> *newNode = new SNode<T>(data, this->front, this->deleteData);

  this->front = newNode;
  if (this->back == NULL)
    this->back = newNode;

  this->size++;

  return this->SUCCESS;
}
template<class T>
Container::Status SList<T>::begin(SListIterator<T>& iterator) {
  iterator.setNode(this->front);
  return this->SUCCESS;
}
template<class T>
Container::Status SList<T>::end(SListIterator<T>& iterator) {
  iterator.setNode(NULL);
  return this->SUCCESS;
}

template class SList<int>;
template class SList<String>;
