// _SNode.h
// Cameron White
// This file contains the iterface of the _SNode class.
#ifndef _SLIST_NODE_H
#define _SLIST_NODE_H
#include "_Node.h"

// This class is a private class. Its purpose is to template key
// data types in order to allow its virtual functions to be valid 
// in any derived version. 
template<class DATA, class NODE> class _SNode
  : public _Node<DATA, NODE> {
 public:
  _SNode();
  _SNode(DATA &data, NODE *next=NULL, 
                   void (*deleteData)(DATA)=NULL);
  // Set the next node.
  typename Container::Status setNext(NODE *node);
  // Get the next node.
  typename Container::Status getNext(NODE **node);
  // Set Node equal to another.
  virtual NODE& operator =(const NODE& rhs);
  // Check if the node is equal to another.
  virtual bool operator ==(const NODE& rhs);
  // Check if the node is not equal to another.
  virtual bool operator !=(const NODE& rhs);
 protected:
  NODE *next;
};

#endif // SLIST_NODE_PRIVATE_H
