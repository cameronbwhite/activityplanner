// dDListIterator.cpp
// Cameron White
// This file contains the implementation of the DListIterator class.
#include "dListIterator.h"
#include "container.h"
#include "string.h"

template<class T> 
DListIterator<T>::DListIterator(DNode<T> *node) 
  : _DListIterator<T, DNode<T>, DListIterator<T> >(node) {
}
template<class T> DListIterator<T> 
DListIterator<T>::operator ++(int) {
  this->node->getNext(&(this->node));
  return *this;
}
template<class T> DListIterator<T> 
DListIterator<T>::operator --(int) {
  this->node->getPrevious(&(this->node));
  return *this;
}
template<class T> T DListIterator<T>::operator *() {
  T *data;
  if (this->node->getData(&data) != Container::SUCCESS)
    ; // TODO Throw exception
  return *data;
}
template<class T> 
bool DListIterator<T>::operator ==(const DListIterator<T>& rhs) {
 if (this->node == rhs.node)
   return true;
 return false;
}
template<class T> 
bool DListIterator<T>::operator !=(const DListIterator<T>& rhs) {
 if (this->node != rhs.node)
   return true;
 return false;
}
template<class T>
DListIterator<T>& DListIterator<T>::operator =(const DListIterator<T> &rhs) {
  this->node = rhs.node;
  return *this;
}

template class DListIterator<int>;
template class DListIterator<String>;
