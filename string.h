// String.h
// Cameron White
// This file contains the String class interface.
#ifndef STRING_H
#define STRING_H

class String {
 public:
  ~String();
  String();
  String(const char *string);
  String(const String &string);
  // Return the Length of the string.
  //
  // The length is the number of characters in the string.
  // The length is returned or -1 if an error occurs.
  // @returns The Length of the string.
  int length() const;
  // Returns the non-editable c-string representation of the string.
  //
  // WARNING: Do NOT edit or deallocate the returned value, unexpected 
  // stuff will occur if you do, which this class will not take any
  // responsibility for.
  // If a editable c-string is desired use the data() method instead.
  // The return value is the c-string (NULL terminated character array)
  // representation of the string object.
  // @returns non-editable c-string
  const char* c_str() const;
  // Returns the c-string representation of the string.
  //
  // Returns an editable c-string representation of the string. Changes
  // to this this does not effect the String, it is a copy. It is the
  // responsibility of the user to deallocate this c-string with delete[].
  // @returns editable c-string
  char *data() const;
  // Set a String equal to a second String.
  //
  // @rhs The String to set the LHS too.
  String& operator =(const String &rhs);
  // Set a String equal to a c-string.
  //
  // @rhs The c-string to set the LHS too.
  String& operator =(const char *rhs);
  // Concatenate two Strings together.
  //
  // @rhs String to concatenate LHS with.
  String operator +(const String &rhs) const;
  // Concatenate a String with a c_string.
  //
  // @rhs c-string to concatenate LHS with.
  String operator +(const char *rhs) const;
  // Concatenate two Strings into the first.
  //
  // @rhs String to concatenate LHS with.
  String& operator +=(const String &rhs);
  // Concatenate a String with a c_string into the String.
  //
  // @rhs c-string to concatenate LHS with.
  String& operator +=(const char *rhs);
  // Evaluate if two Strings are equal.
  //
  // @rhs String to compare the LHS too.
  bool operator ==(const String &rhs) const;
  // Evaluate if a String is equal to a c-string.
  //
  // @rhs c-string to compare the LHS too.
  bool operator ==(const char *rhs) const;
  // Evaluate if two Strings are not equal.
  //
  // @rhs String to compare the LHS too.
  bool operator !=(const String &rhs) const;
  // Evaluate if a String is not equal to a c-string.
  //
  // @rhs c-string to compare the LHS too.
  bool operator !=(const char *rhs) const;
  // Evaluate if the first String is less than the second.
  //
  // @rhs String to compare the LHS too.
  bool operator <(const String &rhs) const;
  // Evaluate if a String is less then a c-string.
  //
  // @rhs c-string to compare the LHS too.
  bool operator <(const char *rhs) const;
  // Evaluate if the first String is less than or equal to the second.
  //
  // @rhs String to compare the LHS too.
  bool operator <=(const String &rhs) const;
  // Evaluate if the String is less than or equal to the c-string.
  //
  // @rhs c-string to compare the LHS too.
  bool operator <=(const char *rhs) const;
  // Evaluate if the first String is greater than the second.
  //
  // @rhs String to compare the LHS too.
  bool operator >(const String &rhs) const;
  // Evaluate if a String is greater than a c-string.
  //
  // @rhs c-string to compare the LHS too.
  bool operator >(const char *rhs) const;
  // Evaluate if the first String is greater than or equal to the
  // second.
  //
  // @rhs String to compare the LHS too.
  bool operator >=(const String &rhs) const;
  // Evaluate if a String is greater than or equal to a c-string.
  //
  // @rhs c-string to compare the LHS too.
  bool operator >=(const char *rhs) const;
 protected:
 private:
  void set(char *string);
  char* concatenate(const String &lfs, const char *rhs) const;
  char *string;
};

#endif // STRING_H

