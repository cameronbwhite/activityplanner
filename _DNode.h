// _DNode.h
// Cameron White
// This file contains the interface of the _DNode class.
// Use this class if want to create the DNode class or a class like
// the DNode class. This class is not meant to be used directly.
#ifndef _D_NODE_H
#define _D_NODE_H

#include "_SNode.h"

// This class is a private class. Its purpose is to template key data
// types in order to allow its virtual functions to be valid in any
// derived version.
template<class DATA, class NODE> class _DNode
  : public _SNode<DATA, NODE> {
 public:
  _DNode();
  _DNode(DATA &data, NODE *previous=NULL, NODE *next=NULL,
                   void (*deleteData)(DATA)=NULL);
  // Sets the previous pointer to the given node. NULL is a valid
  // argument.
  virtual typename Container::Status setPrevious(NODE *node);
  // Retrieves the previous. 
  virtual typename Container::Status getPrevious(NODE **node);
  // Set Node equal to another node.
  virtual NODE& operator =(const NODE& rhs);
  // Check if the Node is equal to another.
  virtual bool operator ==(const NODE& rhs);
  // Check if the Node is equal to another.
  virtual bool operator !=(const NODE& rhs);
 protected:
  NODE *previous;
};

#endif // _D_NODE_H

