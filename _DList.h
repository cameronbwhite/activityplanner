// _DList.h
// Cameron White
// This file contains the interface of the _List class.
// Use this class if you want to create a single list class or a 
// class like a single linked list. This class is not meant to be 
// used directly.
#ifndef _DLIST_H
#define _DLIST_H
#include "_SList.h"

// This class is a private class. Its purpose is to template key
// data types in order to allow its virtual functions to be valid 
// in any derived version. 
template<class DATA, class NODE, class LIST, class ITERATOR> 
class _DList : public _SList<DATA,NODE,LIST,ITERATOR> {
 public:
  _DList(void (*deleteData)(DATA)=NULL);
  //virtual operator =(const LIST& rhs)=0;
 protected:
 private:
};

#endif // _LIST_H

