// _Array.h
// Cameron White
// This file contains the interface of the _Array class.
// Use this Class if you want to create a Container that Stores items
// in a linear way. This class is not meant to be used directly.
#ifndef _ARRAY_H
#define _ARRAY_H
#include "container.h"
#include <cstdlib>

// This class is a private class. Its purpose is to template key
// data types in order to allow its virtual functions to valid in 
// any derived version. 
template<class DATA, class NODE, class LIST, class ITERATOR>
class _Array : public Container {
 public:
  // Default constructor
  _Array(void (*deleteData)(DATA)=NULL);
  // Adds an item to the end of the array.
  virtual Status append(DATA &data)=0;
  // Initialize a iterator to reference the first item in the array.
  virtual Status begin(ITERATOR& iterator)=0;
  // Initialize a iterator to reference the item one past the end of
  // the array.
  virtual Status end(ITERATOR& iterator)=0;
 protected:
  NODE *front;     
  NODE *back;
  void (*deleteData)(DATA);
};

#endif // ARRAYPRIVATE_H

