// _SNode.cpp
// Cameron White
// This file contains the implemention to the SNode class.
#include "sNode.h"
#include "flexibleArray.h"
#include "activity.h"
#include "string.h"

// Default constructor
template<class T>
SNode<T>::SNode() {
}
template<class T>
SNode<T>::SNode(T &data, SNode<T> *next, 
                        void (*deleteData)(T))
  : _SNode<T, SNode<T> >(data, next, deleteData) {
}

template class SNode<int>;
template class SNode<int**>;
template class SNode<Activity**>;
template class SNode<Activity***>;
template class SNode<String>;
