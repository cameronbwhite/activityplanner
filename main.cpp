// main.cpp
// Cameron White
#include <stdio.h>
#include "activity.h"
#include "sCircleList.h"
#include "flexibleArray.h"
#include <readline/readline.h>
#include <readline/history.h>
#include <cstring>

void deleteActivity(Activity *activity) {
  delete activity;
}

int main(int argc, char *argv[]) {
  Activity *activity;
  SCircleList<String> *circleList; 
  FlexibleArray<Activity*, 8> activities = 
    FlexibleArray<Activity*, 8>(deleteActivity);
  SCircleListIterator<String> circleIterator, 
                              circleIteratorStart;
  FlexibleArrayIterator<Activity*, 8> arrayIterator,
                                      arrayIteratorEnd;
  String str;
  char *input;
  char *word;
  bool done;
  
  printf("Starting Activity Chooser Pro\n");

  printf("Loading Activities: ");
  activity = new Activity("Portland Art Museum");
  activity->addInterest("art");
  activities.append(activity);
  activity = new Activity("Portland Trail Blazers");
  activity->addInterest("sports");
  activity->addInterest("basketball");
  activities.append(activity);
  activity = new Activity("Portland Timbers");
  activity->addInterest("sports");
  activity->addInterest("soccer");
  activities.append(activity);
  activity = new Activity("Portland Winterhawnks");
  activity->addInterest("sports");
  activity->addInterest("hockey");
  activities.append(activity);
  activity = new Activity("Mount Hood");
  activity->addInterest("sports");
  activity->addInterest("skiing");
  activity->addInterest("snow");
  activities.append(activity);
  printf("Done\n");

  printf("Posible Interests:\n");
  printf("art, sports, basketball, soccer, hockey, skiing, snow\n");
  
  done = false;
  while(!done) {
    circleList = new SCircleList<String>;
    input = readline("Enter your interests: "); 
    if (strlen(input) <= 0)
      done = true;
    if (!done) {
      word = input;
      word = strtok(word, " ,-.");
      printf("Seleted Interests:\n");
      while (word != NULL) {
        printf("\t%s\n", word);
        str = word;
        circleList->append(str);
        word = strtok(NULL, " ,-.");
      }
      printf("Related activities:\n");
      activities.begin(arrayIterator); 
      activities.end(arrayIteratorEnd); 
      while (arrayIterator != arrayIteratorEnd) {
        activity = *arrayIterator;
        circleList->begin(circleIterator);
        circleList->begin(circleIteratorStart);
        if (circleList->getSize() > 0) {
          do {
            str = *circleIterator;
            if (activity->containsInterest(str)) {
              activity->getName(str);
              printf("\t%s\n", str.c_str());
            }
            circleIterator++;
          } while (circleIterator != circleIteratorStart);
        }
        arrayIterator++;
      }
    }
    free(input);
    delete circleList;
  }

  return 0;
}

