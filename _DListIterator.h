// _DListIterator.h
// Cameron White
// This file contains the interface of the _DListIterator class.
// Use this class if you want to create the DListIterator or a 
// similar class. This class is not meant to be used directly.
#ifndef _D_LIST_ITERATOR_H
#define _D_LIST_ITERATOR_H
#include "bidirectionalIterator.h"
#include "_SListIterator.h"

// This class is a private class. Its purpose is to template key
// data types in order to allow its virtual functions to be valid 
// in any derived version. 
template<class DATA, class NODE, class ITERATOR> 
class _DListIterator 
  : public BidirectionalIterator<DATA, ITERATOR>,
    public _SListIterator<DATA,NODE,ITERATOR> {
 public:
  _DListIterator(NODE *node=NULL);
};

#endif // _D_LIST_ITERATOR_H
