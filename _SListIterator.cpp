// _SListIterator.cpp
// Cameron White
// This file contains the implementation to the _SListIterator
// class.
#include "_SListIterator.h"
#include "sNode.h"
#include "dListIterator.h"
#include "sList.h"
#include "sCircleListIterator.h"
#include "flexibleArrayIterator.h"
#include "flexibleArray.h"
#include "activity.h"
#include "string.h"

// Default Constructor
template<class DATA, class NODE, class ITERATOR> 
_SListIterator<DATA,NODE,ITERATOR> ::_SListIterator(NODE *node) 
  : node(node) {
}
template<class DATA, class NODE, class ITERATOR> 
void _SListIterator<DATA,NODE,ITERATOR>::setNode(NODE *node) {
  this->node = node;
}
template<class DATA, class NODE, class ITERATOR> 
NODE* _SListIterator<DATA,NODE,ITERATOR>::getNode() {
  return this->node;
}
// Template Prototyes
template class _SListIterator<int, SNode<int>, SListIterator<int> >;
template class _SListIterator<int, DNode<int>, DListIterator<int> >;
template class _SListIterator<int, SNode<int>, SCircleListIterator<int> >;
template class _SListIterator<int, SNode<int**>, 
         FlexibleArrayIterator<int, 2> >;
template class _SListIterator<Activity, SNode<Activity**>, 
                              FlexibleArrayIterator<Activity, 8> >;
template class _SListIterator<Activity*, SNode<Activity***>, 
                              FlexibleArrayIterator<Activity*, 8> >;
template class _SListIterator<String, SNode<String>, SListIterator<String> >;
template class _SListIterator<String, DNode<String>, SListIterator<String> >;
template class _SListIterator<String, SNode<String>, DListIterator<String> >;
template class _SListIterator<String, DNode<String>, DListIterator<String> >;
template class _SListIterator<String, SNode<String>, SCircleListIterator<String> >;
