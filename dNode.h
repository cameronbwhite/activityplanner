// dlist.h
// Cameron White
// This file contains the interface of the DNode class.
// Use this class as the node of a DList.
#ifndef D_NODE_H
#define D_NODE_H
#include "_DNode.h"
#include "dList.h"

template<class T> class DList;

// This class is used to implement the List class. All the data 
// stored in the List is stored in individual DNodes. The List 
// node keeps track of the previous and next nodes. The DNode
// deletes the stored data by calling the deleteData function if 
// supplied by the user. More detailed information can be found in
// the parent classes.
template<class T> class DNode 
  : public _DNode<T, DNode<T> > {
 public:
  DNode();
  DNode(T &data, DNode<T> *previous=NULL, 
           DNode<T> *next=NULL, void (*deleteData)(T)=NULL);
};

#endif // D_NODE_H

