// sCircleList.h
// Cameron White
// This file contains the SCircleList class. 
// Use this class to store items is a single linked circular list.
#ifndef SCIRCLELIST_H
#define SCIRCLELIST_H
#include "_SList.h"
#include "sNode.h"
#include "sCircleListIterator.h"
  
// This class is a single linked circular list.
template<class T> class SCircleList 
  : public _SList<T, SNode<T>, SCircleList<T>, 
                  SCircleListIterator<T> > {
 public:
  ~SCircleList();
  SCircleList(void (*deleteData)(T)=NULL);
  // Add data to the end of the circle list.
  virtual Container::Status append(T &data);
  // Add data to the beginning of the circle list.
  virtual Container::Status prepend(T &data);
  // Sets the iterator to reference the start of the list.
  virtual Container::Status begin(SCircleListIterator<T> &iterator);
  // Sets the iterator to reference the end of the list. This is really
  // the same as the begin. I don't really like this.
  virtual Container::Status end(SCircleListIterator<T> &iterator);
 protected:
 private:
};

#endif // SCIRCLELIST_H
