// _DNode.cpp
// Cameron White
// This file contains the implementation of the _DNode class.
#include "_DNode.h"
#include "dNode.h"
#include "sNode.h"
#include "string.h"

template<class DATA, class NODE> 
_DNode<DATA,NODE>::_DNode() 
  : previous(NULL) {
}
template<class DATA, class NODE>
_DNode<DATA,NODE>::_DNode(
    DATA &data, NODE *previous, NODE *next, 
    void (*deleteData)(DATA))
  : _SNode<DATA,NODE>(data, next, deleteData),
    previous(previous) {
}
template<class DATA, class NODE>
typename Container::Status 
_DNode<DATA,NODE>::setPrevious(NODE *previous) {
  this->previous = previous;
  return this->SUCCESS;
}
template<class DATA, class NODE>
typename Container::Status 
_DNode<DATA,NODE>::getPrevious(NODE **previous) {
  if (previous != NULL) {
    *previous = this->previous;
    return this->SUCCESS;
  } else
    return this->NULLPOINTER;
}
template<class DATA, class NODE>
NODE& _DNode<DATA,NODE>::operator =(const NODE &rhs) {
  this->_DNode<DATA,NODE>::operator=(rhs);
  this->previous = rhs.previous;
  // This static_cast does nothing. It is only here so the program
  // will compile.
  return static_cast<NODE&>(*this);
}
template<class DATA, class NODE>
bool _DNode<DATA,NODE>::operator ==(const NODE &rhs) {
  if (this->_DNode<DATA,NODE>::operator ==(rhs))
    if (this->previous == rhs.previous)
      return true;
  return false;
}
template<class DATA, class NODE>
bool _DNode<DATA,NODE>::operator !=(const NODE &rhs) {
  if (this->_DNode<DATA,NODE>::operator !=(rhs))
    return true;
  else if (this->previous != rhs.previous)
    return true;
  return false;
}

// Template Prototypes
template class _DNode<int, DNode<int> >;
template class _DNode<String, DNode<String> >;
