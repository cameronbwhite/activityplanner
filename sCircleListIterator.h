// sCircleListIterator.h
// Cameron White
// This file contains the iterface of the SCircleListIterator class.
#ifndef SCIRCLE_LIST_ITERATOR_H
#define SCIRCLE_LIST_ITERATOR_H
#include "_SListIterator.h"
#include "sNode.h"

// The SCircleListIterator class allows the user to iterate over the
// contents of a SCircleList Container class.  The user must use the
// begin() and end() functions of the SCircleList class.
//
// Example Usage:
//  SCircleList list;
//  SCircleListIterator iterator, iteratorEnd;
//  list.begin(iterator)
//  for(;;) { // WARNING: INFINITE LOOP
//    // Dereference
//    *iterator;
//    // Iterator to the next item.
//    iterator++;
//  }
template<class T> class SCircleListIterator 
  : public _SListIterator<T, SNode<T>, SCircleListIterator<T> > {
 public:
  SCircleListIterator(SNode<T> *node=NULL);
  // Iterate forward
  virtual SCircleListIterator<T> operator ++(int);
  // Dereference 
  //
  // Returns the data stored in the SCircleList at the position
  // indicated by the iterator.  If the iterator is not at a valid
  // position an exception is thrown.
  virtual T operator *();
  // Equality
  virtual bool operator ==(const SCircleListIterator<T> &rhs);
  // Inequality
  virtual bool operator !=(const SCircleListIterator<T> &rhs);
  // Set equal to
  virtual SCircleListIterator<T>& operator =(const SCircleListIterator<T> &rhs);
};

#endif // SCIRCLE_LIST_ITERATOR_H
