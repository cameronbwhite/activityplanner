// iterator.h
// Cameron White
// This file contains the interface of the Iterator class.
// Use this as the parent of all iterators.
#ifndef ITERATOR_H
#define ITERATOR_H

// This is an abstract iterator class.
template<class DATA, class ITERATOR> class Iterator {
 public:
  virtual ITERATOR& operator =(const ITERATOR& rhs) = 0;
  virtual DATA operator *() = 0;
  virtual bool operator ==(const ITERATOR& rhs) = 0;
  virtual bool operator !=(const ITERATOR& rhs) = 0;
 protected:
 private:
};

#endif // ITERATOR_H
