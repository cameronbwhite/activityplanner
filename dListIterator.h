// dListIterator.h
// Cameron White
// This file contains the interface of the DListIterator class.
// Use this class to iterate over the contents of a a DList.
#ifndef D_LIST_ITERATOR_H
#define D_LIST_ITERATOR_H
#include "_DListIterator.h"
#include "dNode.h"
  
template<class T> class DNode;

// The DListIterator class allows the user to iterate over the
// contents of a DList Container class. The user must use the
// begin() and end() functions of the DList class. 
//
// Example Usage:
//  DList list;
//  DListIterator iterator, iteratorEnd;
//  // initilize the iterators
//  array.begin(iterator);
//  array.end(iteratorEnd);
//  // loop over every item in the Container.
//  while (iterator != iteratorEnd) {
//    // Dereference
//    *iterator;
//    // Iterator to the next item.
//    iterator++
//  }
template<class T> class DListIterator 
  : public _DListIterator<T, DNode<T>, DListIterator<T> > {
 public:
  DListIterator(DNode<T> *node=NULL);
  // Iterate forward
  virtual DListIterator<T> operator ++(int);
  // Iterate backwards
  virtual DListIterator<T> operator --(int);
  // Dereference 
  //
  // Returns the data stored in the DList at the position
  // indicated by the iterator.  If the iterator is not at a valid
  // position an exception is thrown.
  virtual T operator *();
  // Equality
  virtual bool operator ==(const DListIterator<T>& rhs);
  // Inequality
  virtual bool operator !=(const DListIterator<T>& rhs);
  // Set equal to
  virtual DListIterator<T>& operator =(const DListIterator<T> &rhs);
};

#endif // D_LIST_ITERATOR_H
