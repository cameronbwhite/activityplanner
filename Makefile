CC = g++
PROGRAM = ActivityChooser
OPTS = -g -Wall -Wno-write-strings -lreadline -lncurses
OBJS = container.o _Array.o _SList.o _DList.o \
			 sList.o dList.o sCircleList.o flexibleArray.o \
			 _Node.o _SNode.o _DNode.o \
			 sNode.o dNode.o \
			 iterator.o forwardIterator.o bidirectionalIterator.o \
			 _SListIterator.o _DListIterator.o \
			 sListIterator.o dListIterator.o sCircleListIterator.o \
			 flexibleArrayIterator.o \
			 string.o activity.o \
			 main.o 

$(PROGRAM): $(OBJS)
	$(CC) -o $@ $(OPTS) $(OBJS)

%.o: %.cpp %.h
	$(CC) -o $@ -c $< $(OPTS)

.PHONY: tests

tests: $(OBJS)
	make -C tests all

.PHONY: clean

clean:
	rm -f *.o *~ .?*.swp $(PROGRAM)
	make -C tests clean

