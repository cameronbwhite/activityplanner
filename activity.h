// activity.h
// Cameron White
// This file contains the iterface to the Activity class.
#ifndef ACTIVITY_H
#define ACTIVITY_H
#include "dList.h"
#include "string.h"

// The Activity class contains information about an activity 
// name, description, and a list of related interests.
class Activity {
 public:
  // Default activity
  Activity();
  Activity(String &name);
  Activity(const char *name);
  ~Activity();
  // Add a related interested.
  void addInterest(const char *interest);
  // Add a related interested.
  void addInterest(String &interest);
  // Set the name of the activity.
  void setName(String &name);
  // Get the name of the activity.
  void getName(String &name);
  // Check if the activity contains the interest.
  bool containsInterest(const char *name);
  // Check if the activity contains the interest.
  bool containsInterest(String &interest);
 private:
  String name;
  DList<String> interests;
};

#endif // ACTIVITY_H
