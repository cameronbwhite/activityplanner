// SListIterator.h
// Cameron White
// This file was contains the interface of the SListIterator class.
#ifndef SLISTITERATOR_H
#define SLISTITERATOR_H
#include "_SListIterator.h"
#include "sNode.h"
#include "sList.h"

template<class T> class SNode;

// The SListIterator class allows the user to iterate over the
// contents of a SList Container class. The user must initialize 
// the iterator begin() and end() from the SList class.
//
// Example Usage:
//  SList list;
//  SListIterator iterator, iteratorEnd;
//  list.begin(iterator);
//  list.end(iteratorEnd);
//  // loop over every item in the Container.
//  while (iterator != iteratorEnd) {
//    // Dereference
//    *iterator;
//    // Iterator to the next item.
//    iterator++;
//  }
template<class T> class SListIterator 
  : public _SListIterator<T, SNode<T>, SListIterator<T> > {
 public:
  SListIterator(SNode<T> *node=NULL);
  // Iterate forward
  virtual SListIterator<T> operator ++(int);
  // Dereference 
  //
  // Returns the data stored in the SList at the position
  // indicated by the iterator.  If the iterator is not at a valid
  // position an exception is thrown.
  virtual T operator *();
  // Equality
  virtual bool operator ==(const SListIterator<T> &rhs);
  // Inequality
  virtual bool operator !=(const SListIterator<T> &rhs);
  // Set equal to
  virtual SListIterator<T>& operator =(const SListIterator<T> &rhs);
};

#endif // SLISTITERATOR_H
