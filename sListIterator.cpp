// SListIterator.cpp
// Cameron White
// This file contains of the implementation SListIterator class.
#include "sListIterator.h"

template<class T> 
SListIterator<T>::SListIterator(SNode<T> *node) 
  : _SListIterator<T, SNode<T>, SListIterator<T> >(node) {
}
template<class T> SListIterator<T>
SListIterator<T>::operator ++(int) {
  this->node->getNext(&(this->node));
  return *this;
}
template<class T> T SListIterator<T>::operator *() {
  T *data;
  if (this->node->getData(&data) != Container::SUCCESS)
    ; // TODO Throw exception
  return *data;
}
template<class T> 
bool SListIterator<T>::operator ==(const SListIterator<T>& rhs) {
 if (this->node == rhs.node)
   return true;
 return false;
}
template<class T> 
bool SListIterator<T>::operator !=(const SListIterator<T>& rhs) {
 if (this->node != rhs.node)
   return true;
 return false;
}
template<class T>
SListIterator<T>& SListIterator<T>::operator =(const SListIterator<T> &rhs) {
  this->node = rhs.node;
  return *this;
}

template class SListIterator<int>;
