// sNode.h
// Cameron White
// This file contains the iterface to the SNode class.
#ifndef SLISTNODE_H
#define SLISTNODE_H
#include "_SNode.h"

template<class T> class SList;

// This class is a singly linked node. Its purpose is to represent 
// the actual nodes stored in the SList class.
template<class T> class SNode 
  : public _SNode<T, SNode<T> > {
 public:
  // Default Constructor
  SNode();
  SNode(T &data, SNode<T> *next=NULL, 
        void (*deleteData)(T)=NULL);
};


#endif // SLISTNODE_H

