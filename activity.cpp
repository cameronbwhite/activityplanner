// activity.cpp
// Cameron White
// This file contains the implementation to the Activity class.
#include "string.h"
#include "activity.h"
#include <stdio.h>
Activity::~Activity() {
}
Activity::Activity() {
}
Activity::Activity(String &name) 
  : name(name) {
}
Activity::Activity(const char *name) 
  : name(name) {
}
// Append the interest to the end of the list of interests.
void Activity::addInterest(const char *interest) {
  String string = interest;
  interests.append(string);
}
// Append the interest to the end of the list of interests.
void Activity::addInterest(String &interest) {
  interests.append(interest);
}
// Wrapper function
bool Activity::containsInterest(const char *name) {
  String string = name;
  return this->containsInterest(string);
}
// Iterate over the list of activities. If the interest is found return
// true if not return false.
bool Activity::containsInterest(String &interest) {
  DListIterator<String> iterator, iteratorEnd;
  interests.begin(iterator);
  interests.end(iteratorEnd);
  String str;
  while (iterator != iteratorEnd) {
    str = *iterator;
    //printf("name=%s\n", name.c_str());
    if (interest == str)
      return true;
    iterator++;
  }
  return false;
}
void Activity::setName(String &name) {
  this->name = name;
}
void Activity::getName(String &name) {
  name = this->name;
}
