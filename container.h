// container.h
// Cameron White
// This file contains the interface of Container class.
// Use this class as a parent of all classes that are used to store
// other class in a datastructure.
#ifndef CONTAINER_H
#define CONTAINER_H

// Container is an abstract like class. It is meant to be derived 
// from. A Container is any class that implements a data structure
// composed of other objects or variables.
class Container {
 public:
   // Return type of most member functions. 
   enum Status {
     SUCCESS,
     UNKNOWN,
     TYPE,
     NULLPOINTER,
   };
   typedef unsigned long SizeType;
  ~Container();
  Container();
  SizeType getSize();
 protected:
  SizeType size;
};

#endif // CONTAINER_H

