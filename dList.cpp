// dList.cpp
// Cameron White
// This file contains the implementation of the DList class.
#include "dList.h"
#include "string.h"

// Iterater over the list deleting each node as it goes.
template<class T>
DList<T>::~DList() {
  DNode<T> *temp;
  // Ensure that iteration stops after the back node.
  if (this->back != NULL)
    this->back->setNext(NULL);
  // Delete every node in the dlist.
  while (this->front) {
    this->front->getNext(&temp);
    delete this->front;
    this->front = temp;
  }
}
// Default Constructor
template<class T>
DList<T>::DList(void (*deleteData)(T)) 
  : _DList<T, DNode<T>, DList<T>, DListIterator<T> >(deleteData) {
}
template<class T>
Container::Status 
DList<T>::append(T &data) {
  DNode<T> *newNode = new DNode<T>(data, 
                                   this->back, // previous * 
                                   NULL,       // next *
                                   this->deleteData);
  if (this->front == NULL)
    this->front = newNode;

  if (this->back != NULL)
    this->back->setNext(newNode);
  this->back = newNode;

  this->size++;

  return this->SUCCESS;
}
template<class T>
Container::Status 
DList<T>::prepend(T &data) {
  DNode<T> *newNode = new DNode<T>(data,            
                                   NULL,        // previous *
                                   this->front, // next *
                                   this->deleteData);
  if (this->front != NULL)
    this->front->setPrevious(newNode);
  this->front = newNode;

  if (this->back == NULL)
    this->back = newNode;
  
  this->size++;

  return this->SUCCESS;
}
template<class T>
Container::Status DList<T>::begin(DListIterator<T>& iterator) {
  iterator.setNode(this->front);
  return this->SUCCESS;
}
template<class T>
Container::Status DList<T>::end(DListIterator<T>& iterator) {
  iterator.setNode(NULL);
  return this->SUCCESS;
}

template class DList<int>;
template class DList<String>;
