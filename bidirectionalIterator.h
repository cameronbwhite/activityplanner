// bidirectionalIterator.h
// Cameron White
// This file contains the interface of the BidirectionalIterator class.
// This class provides a convient way to create a Bidirectional
// iterator.
#ifndef BIDIRECTIONAL_ITERATOR_H
#define BIDIRECTIONAL_ITERATOR_H
#include "forwardIterator.h"

// This is an abstract bidirectional iterator class. Derive an
// iterator from this class if you want an iterator to be able to
// iterate in both directions.
template<class DATA, class ITERATOR> class BidirectionalIterator 
  : public ForwardIterator<DATA,ITERATOR> {
 public:
  virtual ITERATOR operator --(int) = 0;
};

#endif // BIDIRECTIONAL_ITERATOR_H
