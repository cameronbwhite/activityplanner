// bidirectionalIterator.cpp
// Cameron White
// This the contains the implementation of the BidirectionalIterator
// class.
#include "bidirectionalIterator.h"
#include "dListIterator.h"
#include "dNode.h"
#include "_DListIterator.h"
#include "sCircleListIterator.h"

template class BidirectionalIterator<int, DListIterator<int> >;
template class BidirectionalIterator<int, 
  _DListIterator<int, DNode<int>, DListIterator<int> > >;
