// container.cpp
// Cameron White
// This file contains the implementation of Container class.
#include "container.h"

Container::~Container() {
}
Container::Container() 
  : size(0) {
}
Container::SizeType Container::getSize() {
  return size;
}
