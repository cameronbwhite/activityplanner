// sCircleListIterator.cpp
// Cameron White
// This file contains the implementation of the SCircleListIterator 
// class.
#include "sCircleListIterator.h"
#include "string.h"

template<class T> 
SCircleListIterator<T>::SCircleListIterator(SNode<T> *node) 
  : _SListIterator<T, SNode<T>, SCircleListIterator<T> >(node) {
}
template<class T> 
SCircleListIterator<T> SCircleListIterator<T>::operator ++(int) {
  this->node->getNext(&(this->node));
  return *this;
}
template<class T> T SCircleListIterator<T>::operator *() {
  T *data;
  if (this->node->getData(&data) != Container::SUCCESS)
    ; // TODO Throw exception
  return *data;
}
template<class T> 
bool SCircleListIterator<T>::operator ==(const SCircleListIterator<T> &rhs) {
 if (this->node == rhs.node)
   return true;
 return false;
}
template<class T> 
bool SCircleListIterator<T>::operator !=(const SCircleListIterator<T> &rhs) {
 if (this->node != rhs.node)
   return true;
 return false;
}
template<class T> SCircleListIterator<T>& 
SCircleListIterator<T>::operator =(const SCircleListIterator<T> &rhs) {
  this->node = rhs.node;
  return *this;
}

template class SCircleListIterator<int>;
template class SCircleListIterator<String>;
