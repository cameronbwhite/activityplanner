// Node.h
// Cameron White
// This file contains the interface the _Node class.
// Use this class if you want to create a Node class. This class in not
// meant to be used directly.
#ifndef _NODE_H
#define _NODE_H
#include "container.h"
#include <cstdlib>

// Node is an abstract like class. It is meant to be derived from.
// The class stores data and allows the user to get and set this date.
// The class can store "statically" or "dynamically" allocated data. 
// To store data statically specify a non-pointer type in the class 
// template. To store data dynamically specify a pointer type which 
// points to dynamic memory. When the data is change or when the class
// deconstructs the default behavior is to do nothing with the stored
// data. If you stored dynamic data and wish to have the data 
// automatically deallocated specify a deleteData function in the 
// constructor.
template<class DATA, class NODE> class _Node : public Container {
 public:
  ~_Node();
  // Default constructor
  _Node(void (*deleteData)(DATA)=NULL);
  _Node(DATA &data, void (*deleteData)(DATA));
  // Retrives tha data stored in the node
  Status getData(DATA &data);
  // Retrives tha data stored in the node
  Status getData(DATA **data);
  // Sets the data stored in the node.
  Status setData(DATA &data);
  // Sets the data stored in the node.
  Status setData(DATA *data);
  // Set Node equal to another.
  virtual NODE& operator =(const NODE &rhs)=0;
  // Check if the node is equal to another.
  virtual bool operator ==(const NODE &rhs)=0;
  // Check if the node is not equal to another.
  virtual bool operator !=(const NODE &rhs)=0;
 protected:
  DATA data;
  void (*deleteData)(DATA);
};

#endif // NODE_PRIVATE_H
