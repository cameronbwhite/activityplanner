// iterator.cpp
// Cameron White
// This file contains the implementation of the Iterator class.
#include "iterator.h"
#include "sListIterator.h"
#include "dListIterator.h"
#include "_DListIterator.h"
#include "_SListIterator.h"
#include "dNode.h"
#include "flexibleArrayIterator.h"
#include "flexibleArray.h"
#include "activity.h"

// Template Prototypes
template class Iterator<int, SListIterator<int> >;
template class Iterator<int, 
         _SListIterator<int, SNode<int>, SListIterator<int> > >;
template class Iterator<int, DListIterator<int> >;
template class Iterator<int, 
         _DListIterator<int, DNode<int>, DListIterator<int> > >;
template class Iterator<int,
         _SListIterator<int, SNode<int**>, FlexibleArrayIterator<int, 2> > >;
template class Iterator<Activity,
         _SListIterator<Activity, SNode<Activity**>, 
                        FlexibleArrayIterator<Activity,8> > >;
template class Iterator<Activity*,
         _SListIterator<Activity*, SNode<Activity***>, 
                        FlexibleArrayIterator<Activity*,8> > >;
