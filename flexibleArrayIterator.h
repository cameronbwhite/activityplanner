// flexibleArrayIterator.h
// Cameron White
// This file contains the iterface of the 
// FlexibleArrayIterator class.
// Use this to iterate over the contents of the array.
#ifndef FLEXABLE_ARRAY_ITERATOR_H
#define FLEXABLE_ARRAY_ITERATOR_H
#include "_SListIterator.h"
#include "sNode.h"

// The FlexibleArrayIterator class allows the user to iterate over
// the contents of a FlexibleArray Container class.  The user 
// never must use the begin() and end() functions of the the
// FlexibleArray Class to initilize the objects of this type.
// 
// Example Usage:
//  FlexibleArray array;
//  FlexibleArrayIterator iterator, iteratorEnd;
//  // initilize the iterators 
//  array.begin(iterator);
//  array.end(iteratorEnd);
//  // loop over every item in the Container.
//  while (iterator != iteratorEnd) {
//    // Dereference
//    *iterator;
//    // Iterator to the next item.
//    iterator++;
//  }
template<class T, int N> class FlexibleArrayIterator 
  : public _SListIterator<T, SNode<T**>, FlexibleArrayIterator<T,N> > {
 public:
  FlexibleArrayIterator();
  virtual FlexibleArrayIterator<T,N> operator ++(int);
  // Dereference 
  //
  // Returns the data stored in the FlexibleArray at the position
  // indicated by the iterator.  If the iterator is not at a valid
  // position an exception is thrown.
  virtual T operator *();
  // Equality
  virtual bool operator ==(const FlexibleArrayIterator<T,N> &rhs);
  // Inequality
  virtual bool operator !=(const FlexibleArrayIterator<T,N> &rhs);
  // Set equal to
  virtual FlexibleArrayIterator<T,N>& operator =(
      const FlexibleArrayIterator<T,N> &rhs);
  unsigned getIndex();
  void setIndex(unsigned index);
 protected:
  unsigned index;
};

#endif // FLEXABLE_ARRAY_ITERATOR_H

