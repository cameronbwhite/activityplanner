// _DList.cpp
// Cameron White
#include "_DList.h"
#include "sList.h"
#include "dList.h"
#include "sListIterator.h"
#include "string.h"

// Default Constructor
template<class DATA, class NODE, class LIST, class ITERATOR> 
_DList<DATA,NODE,LIST,ITERATOR>::_DList(void (*deleteData)(DATA))
  : _SList<DATA,NODE,LIST,ITERATOR>(deleteData) {
}

// Template Prototypes
template class _DList<int, DNode<int>, DList<int>, 
                      DListIterator<int> >;
template class _DList<String, DNode<String>, DList<String>, 
                      DListIterator<String> >;
