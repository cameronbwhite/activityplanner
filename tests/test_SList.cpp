// test_SList.cpp
// Cameron White
#include "../sList.h"
#include "../sListIterator.h"
#include <stdio.h>

void test_1() {
  SList<int> slist;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i4 = 1;
  int i5 = 2;
  int i6 = 3;
  slist.append(i1);
  slist.append(i2);
  slist.append(i3);
  slist.prepend(i4);
  slist.prepend(i5);
  slist.prepend(i6);
}
void test_2() {
  SList<int> slist;
  SListIterator<int> iterator;
  SListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  slist.append(i1);
  slist.append(i2);
  slist.append(i3);
  slist.begin(iterator);
  slist.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}
void test_3() {
  SList<int> slist;
  SListIterator<int> iterator;
  SListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  slist.prepend(i1);
  slist.prepend(i2);
  slist.prepend(i3);
  slist.begin(iterator);
  slist.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}
void test_4() {
  SList<int> slist;
  SListIterator<int> iterator;
  SListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  slist.prepend(i1);
  slist.prepend(i2);
  slist.prepend(i3);
  slist.append(i1);
  slist.append(i2);
  slist.append(i3);
  slist.begin(iterator);
  slist.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}
void test_5() {
  SList<int> slist;
  SListIterator<int> iterator;
  SListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  slist.append(i1);
  slist.append(i2);
  slist.append(i3);
  slist.prepend(i1);
  slist.prepend(i2);
  slist.prepend(i3);
  slist.begin(iterator);
  slist.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}

int main() {
  printf("Test 1\n");
  test_1();
  printf("Test 2\n");
  test_2();
  printf("Test 3\n");
  test_3();
  printf("Test 4\n");
  test_4();
  printf("Test 5\n");
  test_5();
}
