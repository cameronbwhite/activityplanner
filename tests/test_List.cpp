// test_list.cpp
// Cameron White
#include "../dList.h"
#include <stdio.h>

void test_1() {
  DList<int> list;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i4 = 1;
  int i5 = 2;
  int i6 = 3;
  list.append(i1);
  list.append(i2);
  list.append(i3);
  list.prepend(i4);
  list.prepend(i5);
  list.prepend(i6);
}
void test_2() {
  DList<int> list;
  DListIterator<int> iterator;
  DListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  list.append(i1);
  list.append(i2);
  list.append(i3);
  list.begin(iterator);
  list.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}
void test_3() {
  DList<int> list;
  DListIterator<int> iterator;
  DListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  list.prepend(i1);
  list.prepend(i2);
  list.prepend(i3);
  list.begin(iterator);
  list.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}
void test_4() {
  DList<int> list;
  DListIterator<int> iterator;
  DListIterator<int> iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i4 = 1;
  int i5 = 2;
  int i6 = 3;
  int i;
  list.append(i1);
  list.append(i2);
  list.append(i3);
  list.prepend(i4);
  list.prepend(i5);
  list.prepend(i6);
  list.begin(iterator);
  list.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
  }
}

int main() {
  printf("Test 1\n");
  test_1();
  printf("Test 2\n");
  test_2();
  printf("Test 3\n");
  test_3();
  printf("Test 4\n");
  test_4();
}
