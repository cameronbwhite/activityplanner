// tests_CircleList.cpp
#include "../sCircleList.h"
#include <stdio.h>

void test_1() {
  SCircleList<int> list;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i4 = 1;
  int i5 = 2;
  int i6 = 3;
  list.append(i1);
  list.append(i2);
  list.append(i3);
  list.prepend(i4);
  list.prepend(i5);
  list.prepend(i6);
}
void test_2() {
  SCircleList<int> list;
  SCircleListIterator<int> iterator, iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i, count;
  list.append(i1);
  list.append(i2);
  list.append(i3);
  list.begin(iterator);
  list.end(iteratorEnd);
  count = 0;
  while (count < 10) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
    count++;
  }
}
void test_3() {
  SCircleList<int> list;
  SCircleListIterator<int> iterator, iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i, count;
  list.prepend(i1);
  list.prepend(i2);
  list.prepend(i3);
  list.begin(iterator);
  list.end(iteratorEnd);
  count = 0;
  while (count < 10) {
    i = *iterator;
    printf("%d\n", i);
    iterator++;
    count++;
  }
}

int main(int argc, char *argv[]) {
  printf("Test 1\n");
  test_1();
  printf("Test 2\n");
  test_2();
  printf("Test 3\n");
  test_3();
  /*
  printf("Test 4\n");
  test_4();
  */
}

