// test_flexibleArray.cpp
// Cameron White
#include "../flexibleArray.h"
#include <stdio.h>

void test_1() {
  FlexibleArray<int, 2> array;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i4 = 1;
  int i5 = 2;
  int i6 = 3;
  array.append(i1);
  array.append(i2);
  array.append(i3);
  array.append(i4);
  array.append(i5);
}
void test_2() {
  FlexibleArray<int, 2> array;
  FlexibleArrayIterator<int, 2> iterator, iteratorEnd;
  int i1 = 1;
  int i2 = 2;
  int i3 = 3;
  int i;
  array.append(i1);
  array.append(i2);
  array.append(i3);
  array.begin(iterator);
  array.end(iteratorEnd);
  while (iterator != iteratorEnd) {
    i = *iterator; 
    printf("i=%d\n", i);
    iterator++;
  }
}

int main() {
  printf("Test 1\n");
  test_1();
  printf("Test 2\n");
  test_2();
}
