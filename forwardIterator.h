// forwardIterator.h
// Cameron White
// This file contains the interface of the ForwardIterator class.
// Use this file as a parent to a forward iterator.
#ifndef FORWARD_ITERATOR_H
#define FORWARD_ITERATOR_H
#include "iterator.h"

// This is an abstract ForwardIterator class. Derive from this
// iterator if you want to a iterator that can only iterate in one
// direction. 
template<class DATA, class ITERATOR> class ForwardIterator : 
  public Iterator<DATA,ITERATOR> {
 public:
  virtual ITERATOR operator ++(int) = 0;
};

#endif // FORWARD_ITERATOR_H
