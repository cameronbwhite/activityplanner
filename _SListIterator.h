// _SListIterator.h
// Cameron White
// This file contains the interface to the _SListIterator class.
#ifndef _SLIST_ITERATOR_H
#define _SLIST_ITERATOR_H
#include "forwardIterator.h"
#include <cstdlib>

// This class is a private class. Its purpose is to template key
// data types in order to allow its virtual functions to be valid 
// in any derived version. 
template<class DATA, class NODE, class ITERATOR> 
class _SListIterator : public ForwardIterator<DATA, ITERATOR> {
  public:
   // Default Constructor
   _SListIterator(NODE *node=NULL);
   // Set the node the iterator references.
   void setNode(NODE *node);
   // Return the node referenced by the iterator.
   NODE* getNode();
  protected:
   NODE *node; 
};

#endif // _SLIST_ITERATOR_H
