// sFlexibleArray.cpp
// Cameron White
// This file contains the implementation of the FlexibleArray class.
// TODO This whole this is a mess. I don't Like this file at all.
#include "flexibleArray.h"
#include "activity.h"
#include <cstring>

// TODO this is a mess
template<class T, int N> FlexibleArray<T,N>::~FlexibleArray() {
  SNode<T**> *temp;
  // Ensure that iteration stops after the back node.
  if (this->back != NULL)
    this->back->setNext(NULL);
  // Delete every node in the list.
  while (this->front) {
    this->front->getNext(&temp);
    T **dataArray;
    this->front->getData(dataArray);
    for (int i = 0; i < N; i++) {
      if (this->deleteData && dataArray[i] != NULL)
        this->deleteData(*dataArray[i]);
      delete dataArray[i];
    }
    delete[] dataArray;
    delete this->front;
    this->front = temp;
  }
}
template<class T, int N> FlexibleArray<T,N>::FlexibleArray(
    void (*deleteData)(T)) 
  : _Array<T,SNode<T**>,FlexibleArray<T,N>,FlexibleArrayIterator<T,N> >(deleteData) {
}
template<class T, int N>
Container::Status FlexibleArray<T,N>::append(T &data) {
  T **dataArray;
  if (this->front == NULL || this->back == NULL) {
    this->front = createNode(NULL);
    this->back = this->front;
  }

  this->back->getData(dataArray); 
  int i;
  for (i = 0; i < N; i++) {
    if (dataArray[i] == NULL) {
      setDataArray(data, dataArray, i);
      break;
    }
  }
  if (i == N) {
    SNode<T**> *newNode;
    newNode = createNode(NULL);
    newNode->getData(dataArray);
    setDataArray(data, dataArray, 0);
    this->back->setNext(newNode);
    this->back = newNode;
  }
  
  this->size++;

  return this->SUCCESS;
}
template<class T, int N>
Container::Status FlexibleArray<T,N>::begin(FlexibleArrayIterator<T,N> &iterator) {
  iterator.setNode(this->front);
  iterator.setIndex(0);
  return this->SUCCESS;
}
template<class T, int N>
Container::Status FlexibleArray<T,N>::end(FlexibleArrayIterator<T,N> &iterator) {
  iterator.setNode(NULL);
  iterator.setIndex(0);
  return this->SUCCESS;
}
template<class T, int N>
SNode<T**>* FlexibleArray<T,N>::createNode(SNode<T**> *next) {
  T **dataArray = new T*[N];
  memset(dataArray, 0, sizeof(dataArray)*N);
  SNode<T**> *newNode = new SNode<T**>(dataArray, next, NULL);
  return newNode;
}
template<class T, int N>
Container::Status FlexibleArray<T,N>::setDataArray(T &data, T **dataArray, int i) {
  dataArray[i] = new T;
  *dataArray[i] = data;
  return this->SUCCESS;
}

template class FlexibleArray<int, 2>;
template class FlexibleArray<Activity*, 8>;
template class FlexibleArray<Activity, 8>;
