// forwardIterator.cpp
// Cameron White
// This file contains the implementaion of the ForwardIterator Class.
#include "forwardIterator.h"
#include "sListIterator.h"
#include "_SListIterator.h"
#include "dListIterator.h"
#include "dNode.h"
#include "_DListIterator.h"
#include "flexibleArrayIterator.h"
#include "activity.h"

// Template Prototypes
template class ForwardIterator<int, SListIterator<int> >;
template class ForwardIterator<int, 
         _SListIterator<int, SNode<int>, SListIterator<int> > >;
template class ForwardIterator<int, DListIterator<int> >;
template class ForwardIterator<int, 
         _DListIterator<int, DNode<int>, DListIterator<int> > >;
template class ForwardIterator<int, 
         _SListIterator<int, SNode<int**>, FlexibleArrayIterator<int, 2> > >;
template class ForwardIterator<Activity, 
         _SListIterator<Activity, SNode<Activity**>, 
            FlexibleArrayIterator<Activity, 8> > >;
template class ForwardIterator<Activity*, 
         _SListIterator<Activity*, SNode<Activity**>, 
            FlexibleArrayIterator<Activity*, 8> > >;
